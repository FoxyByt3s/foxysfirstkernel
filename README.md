# foxysFirstKernel
 x86 kernel that boots and displays text 
 based on the tutorial Kernels 101 – Let’s write a Kernel : Arjun Sreedharan
 Link : https://arjunsreedharan.org/post/82710718100/kernels-101-lets-write-a-kernel

 Modifications from Michael Petch on stackoverflow
 Link : https://stackoverflow.com/questions/34687608/unrecognised-emulation-mode-elf-i386-on-mingw32
