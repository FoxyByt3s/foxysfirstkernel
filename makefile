kernel: kasm.o kc.o
	ld  -m elf_i386 -T link.ld -o kernel kasm.o kc.o -build-id=none
kc.o: kernel.c
	gcc -m32 -c kernel.c -o kc.o -ffreestanding -nostdlib -nostdinc	
kasm.o: kernel.asm
	nasm -f elf32 kernel.asm -o kasm.o